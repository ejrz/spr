# SPR - Simple Project [Backend]

this project contains the file structure of backend using:

python 3.10.2 + FastAPI + Alembic + SLQAlchemy + sqlmodel (FastAPI modelling) 

## Usage
To run the server on terminal, make sure you have installed the 
python packages, and you're on the root directory of the project.
```commandline
uvicorn src.main:app --reload
```
Access ```localhost:8000/docs``` to test the endpoints on swagger. 

On run the FastAPI server will check and create database ```prdb```
if the database do not exist.

## Installation
Install and setup ```PostgreSQL```

Clone the project.

Create python virtual environment
```commandline
python3 -m venv venv
```
Activate the venv for powershell or simply remove the ```.ps1``` 
extension when using command prompt.
```
.\venv\Scripts\activate.ps1 
```

Run on the command prompt make sure you're on the root directory.
```commandline
pip install -r requirements.txt
``` 


Edit the ```SPR > db_config.ini``` 
depending on your local machine PostgreSQL setup.

Edit the ```SPR > alembic.ini``` 
depending on your local machine PostgreSQL setup.

**Note:**

Running **all** alembic migration _on initial run_ are still in progress.

## License
[MIT](https://choosealicense.com/licenses/mit/)