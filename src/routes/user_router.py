from fastapi import APIRouter
from src.initializer import Database
from src.database.transactions import Transactions

from src.models.users import Users
from src.models.user_roles import UserRoles

db = Database()
trans = Transactions(db.session())

user_router = APIRouter(
	prefix="/api/user",
	tags=["user"]
)


@user_router.get("/")
async def index():
	return {"route": "user router"}


@user_router.get("/select/users_list")
async def select_users():
	return trans.select(Users)


@user_router.get("/select/user_role_list")
async def select_user_roles():
	return trans.select(UserRoles)


@user_router.post("/insert/user", status_code=201)
async def insert_user(user: Users):
	trans.insert(user)
	return user


@user_router.post("/insert/user_role", status_code=201)
async def insert_user_role(user_role: UserRoles):
	trans.insert(user_role)
	return user_role


