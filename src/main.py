from fastapi import FastAPI, status
from src.initializer import Database
from .routes.user_router import user_router

app = FastAPI()
app.include_router(user_router)

db = Database()
db.create()


@app.get("/", status_code=status.HTTP_200_OK)
async def index():
	return {"Message": "Index"}
