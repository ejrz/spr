from sqlmodel import Field, DateTime
from datetime import datetime


class AuditProperty:

	def __init__(self) -> None:
		pass

	def created_date(self) -> Field:
		createdDate = Field(
			sa_column = DateTime(timezone=False),
			nullable = False,
			default = datetime.utcnow()
		)

		return createdDate
	