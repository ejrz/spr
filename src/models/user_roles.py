from typing import Optional, List
from sqlmodel import SQLModel, Field, Relationship, DateTime
from datetime import datetime

from src.models.audit_property import AuditProperty
from src.models.users import *


class UserRoles(SQLModel, table=True):
	__tablename__: str = "user_roles"
	id: Optional[int] = Field(default=None, primary_key=True)
	role: str
	created_date: datetime = Field(
		sa_column=DateTime(timezone=False),
		nullable=False,
		default=datetime.utcnow()
	)
	created_by: int
	
	# users: List["users"] = Relationship(back_populates="user_roles")

