from typing import Optional, List
from sqlalchemy import Column, String
from sqlmodel import Relationship, SQLModel, Field, DateTime
from datetime import datetime

from src.models.audit_property import AuditProperty
from src.models.user_roles import *


class Users(SQLModel, table=True):
	__tablename__: str = "users"
	id: Optional[int] = Field(default=None, primary_key=True)
	firstname: str = Field(max_length=255)
	lastname: str = Field(max_length=255)
	email: str = Field(index=True, sa_column=Column('email', String, unique=True))
	password: str = Field(max_length=512)
	user_role_id: Optional[int] = Field(foreign_key="user_roles.id", index=True)
	created_date: datetime = Field(
		sa_column=DateTime(timezone=False),
		nullable=False,
		default=datetime.utcnow()
	)
	created_by: int

	# user_role: List["user_roles"] = Relationship(back_populates="users")
