from sqlmodel import Session, select


class Transactions:
	__Session = None

	def __init__(self, sess: Session):
		self.__Session = sess

	def insert(self, obj):
		with self.__Session as session:
			session.add(obj)
			session.commit()

	def select(self, obj):
		with self.__Session as session:
			query = select(obj)
			result = session.exec(query)
			return result.all()
	