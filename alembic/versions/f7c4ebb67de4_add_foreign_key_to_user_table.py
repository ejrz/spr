"""Add foreign_key to User_table

Revision ID: f7c4ebb67de4
Revises: be053181a4b6
Create Date: 2022-03-26 16:47:43.114251

"""
from alembic import op
import sqlalchemy as sa
import sqlmodel


# revision identifiers, used by Alembic.
revision = 'f7c4ebb67de4'
down_revision = 'be053181a4b6'
branch_labels = None
depends_on = None


def upgrade():
	op.create_foreign_key(
		constraint_name="fk_user_role_id",
		source_table="users",
		referent_table="user_roles",
		local_cols=["user_role_id"],
		remote_cols=["id"]
	)


def downgrade():
	op.drop_constraint(
		constraint_name="fk_user_role_id",
		table_name="users",
		type_="foreignkey"
	)
